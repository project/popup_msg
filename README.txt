
Summary
-------
Drupal has perfect status/error message collect process,
but only have a basic UI. This tiny module will improve 
user experience when they try to using drupal.

The default drupal messages display on the top of content,
many user will ignore them when they don't want to wait 
page loading and scroll down to see what's new of site.
So this module provide a popup box to tell user "the 
status/error message is here!", lock the scroll of window 
before they close the box. Box will auto close in seconds.
And user can see messages any time they click bottom 
sticky notice bar.


Features summary
----------------
1. Popup box to attract user's eye.
2. Transparent background.
3. Auto close in customizable seconds.
4. Notice bar will fixed position in the bottom of page when ever user scroll.
5. Graceful degrade if JS not enabled
6. Cross browser - Tested in ie6+, ff3.0+, google chrome, safari 3.1+


Installation
------------
To install this module, do the following:

1. copy to module directory (sites/all/modules).
2. enable module
3. check setting page to customize.



Bugs/Features/Patches
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.


Author
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)

If you use this module, find it useful, and want to send the author
a thank you note, feel free to contact me.

The author can also be contacted for paid customizations of this
and other modules.

