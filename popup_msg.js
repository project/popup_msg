if (Drupal.jsEnabled) {
  $(document).ready(function(){
    // initialize and set global vars
    var settings = Drupal.settings.popup_msg;
    var timeout = settings.timeout;
    var message = $(settings.selector);
    var close = document.createElement('div');
    var messageBox = document.createElement('div');
    var tip = document.createElement('p');

    // fix strenge ie issue
    var oldHeight = 0;

    // function for detect toggle
    var isshow = function(o){
      if($(o).css('display') == 'none') return 0;
      else return 1;
    }

    var fixedBox = function(o){
      var vOffset = (document.documentElement.scrollTop || document.body.scrollTop) + $(window).height() - $(o).height();
      $(o).css({top: vOffset +'px'});
    
    }
    var togglebox = function(o){
      if(isshow(o)){
        $('.messages .close > div').remove();
        $(o).fadeOut('fast');
        $(tip)
          .css('background-color', '#222')
          .show()
          .animate({opacity: 0.7}, 1000 );
        // fix strenge bug for height() issue
        if($.browser.msie){
          oldHeight = $(o).height();
          $(o).height(0);
          fixedBox(messageBox);
        }
      }
      else{
        $(o).fadeIn('fast');
        $('.tip').hide();
        // fix strenge bug for height() issue
        if($.browser.msie){
          $(tip).height(0);
          var nowTop = $(messageBox).offset().top - oldHeight + $(o).height();
          $(messageBox).css({top: nowTop + 'px'});
        }
      }

    }

    // main function
    if($(message).html() != null){
      $(close)
        .html(settings.msgok +'<div>'+ settings.msgclose + ' <span>'+ timeout +'</span>s</div>')
        .addClass('close')
        .prependTo(message);

      $(message)
        .css({width:settings.boxwidth, cursor:'pointer'})
        .addClass('popupmsg')
        .attr('id','popupmsg')
        .appendTo(messageBox);

      $(tip)
        .html(settings.msgshowbox)
        .addClass('tip')
        .hide()
        .appendTo(messageBox);
      
      $(messageBox).animate({opacity: 0.8}, 1000 )
      $(messageBox)
        .attr('id', 'messagebox')
        .addClass('messagebox')
        .appendTo('body');

      timeout++;
      var closebox = function(){
        if(isshow(message)){
          timeout--;
          if(timeout <= 0){
            togglebox(message);
            clearTimeout(meter);
          }
          $('.close span').html(timeout);
          meter = setTimeout(closebox, 1000);
        }
      }

      closebox();
      
      // event handler
      $(message).click(function(){
        if(isshow(message)){
          togglebox(message);
        }
      });

      if($.browser.msie){
        $(messageBox).css('position', 'absolute');
        $(window).scroll(function(){
          if(isshow(message) || isshow(tip)){
            fixedBox(messageBox);
          }
        });
      }
      $(tip).click(function(){
        if(!isshow(message)){
          togglebox(message);
        }
      });
    }
  });
}


  
